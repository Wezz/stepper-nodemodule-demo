# Stepper in a demo project

Angular stepper version 3

This is the guide to get started using the re-usable stepper component.

In the past the stepper had to be built from the ground up as it was crafted for a specific context/project. What we have tried to do is create a stepper which comes standard with basic functionality and saves developers time.

The stepper and other components were built in :
Angular CLI: 8.3.9     Node: 10.16.2     Angular: 8.2.11


## How the stepper works

An application may need to load new components/steps at runtime.
We have used the ComponentFactoryResolver to add components dynamically within the stepper.
Before we could add components we had to define an anchor point to tell Angular where to insert the step components.
The stepper uses a helper directive called StepDirective to mark valid insertion points in the template.

```
 import { Directive, ViewContainerRef } from '@angular/core';
 @Directive({
  selector: '[step-host]',
 })
 export class StepDirective {
  constructor(public viewContainerRef: ViewContainerRef) { }
 }

StepDirective injects ViewContainerRef to gain access to the view container of the element that will host the dynamically added component.
```



Inside the stepper component we have added the directive to a ng-template element.

```
 <div class="stepper-content">
   <ng-template step-host>
   </ng-template>
 </div>
```

- StepperComponent takes an array of StepItem objects as input, which ultimately comes from StepperService.
- StepItem objects specify the type of component to load and any data to bind to the component.
- StepperService returns the actual steps.
- Passing an array of components to StepperComponent allows for a dynamic list of steps without static elements in the template.
- With a method called getSteps(), StepperComponent is able to cycle through the array of StepItems and load a new component by calling loadComponent().
- After loadComponent() selects a step, it uses ComponentFactoryResolver to resolve a ComponentFactory for each specific component. The ComponentFactory then creates an instance of each component.
- Next we target the viewContainerRef that exists on this specific instance of the component. 
- Finally we call createComponent() on ViewContainerRef.


In the Stepper, all components implement a common StepComponent interface to standardize the API for passing data to the components.

```
 export interface StepComponent {
   data: any;
 }
```

Now that you understand how the stepper is built the next section you will see how to implement the stepper component in your project.


## Getting started
### Step 1 - Node modules
First thing you will need to do is install the stepper node module:

**Use these commands:**
npm i nedbank-design // locally  npm i dist/nedbank-design-0.0.1.tgz

This will install all our Nedbank Ui components and allow you to use the ui-stepper selector in your application.

The stepper is used in conjunction with on other UI components: Modal and buttons.

Then any UI components needed in the flow need to be installed see the DSM site for commands.  - https://nedbank.invisionapp.com/dsm/nedbank/nedbank-web

Once installed you import the modules into your main ngModule

**app.module.ts**

```
import { StepperModule, ModalModule, ButtonModule, otherModules } from 'nedbank-design';

```

And of course reference the modules in the imports array.

To ensure that the compiler generates a factory of steps, add dynamically loaded components to the NgModule's entryComponents array:

`  entryComponents: [ Step1Component, Step2Component],`

### Step 2 - Stepper service and dynamic components

You will then need to create a service which will serve the different steps.

Start by importing step-items from library and the necessary components.


**yourService.service.ts**

```
import { Injectable } from '@angular/core';
import { StepItem } from 'nedbank-design';

//Your exsisting components or components to add in stepper
import { Step1Component } from './../app/step1/step1.component';
import { Substep1Component } from './../app/substep1/substep1.component';
import { Subsubstep1Component } from './../app/subsubstep1/subsubstep1.component';
import { Step2Component } from './../app/step2/step2.component';
```


Next in the export class of the service add a getSteps() method and return each step.

The stepper can have up to 3 levels of nested links in the side navigation, remember to structure the items based on which level in the side navigation links you need the component to be shown.


**yourService.service.ts**

```
export class StepperService {
 
  getSteps() {
    return [
      new StepItem(Step1Component,
      { uniqueId: 'step1main',
        order: 1,
        mainStep: true,
        stepName: 'Step 1',
        buttonText: 'Next to Sub step 1',
        active: false,
        disabled: false,
        mainDropdown: true,
        lastItem: false
        
      }),
      new StepItem(Substep1Component,
        { subStepLevel1: true,
          mainStepDisabled: false,
          MainstepName: 'Step 1',
          mainStepUniqueId: 'step1main',
          stepName: 'Sub step 1',
          buttonText: 'Next to step Sub sub step 1',
          active: false,
          disabled: false,
          dropdown: true,
          lastItem: false
      }),
      new StepItem(Substep1Component,
      { subStepLevel2: true,
        mainStepDisabled: false,
        MainstepName: 'Step 1',
        mainStepUniqueId: 'step1main',
        stepName: 'Sub sub step 1',
        buttonText: 'Next to step 2',
        active: false,
        disabled: false,
        dropdown: true,
        lastItem: false
      }),
      new StepItem(Step2Component,
      { uniqueId: 'step2main',
        order: 2,
        mainStep: true,
        stepName: 'Step 2',
        buttonText: 'Next to Sub step 2',
        active: false,
        disabled: false,
        mainDropdown: true,
        lastItem: true
      })
    ];
  }
```


Properties in objects explained:

**uniqueId -** This is only needed on the main steps and not sublevel 1 or 2 steps (Is the actual Id set on element).

**order -** This is only needed on the main steps and not sublevel 1 or 2 steps (Is the order of main step).

**mainStep -** This should be set to true for main or first level steps.

**subSteplevel1 -** Is this a sub step of main step

**subSteplevel2 -** Is this a sub step of sub level 1 step.

**stepName -** The name of the actual step.

**mainstepName -** Refers to the main step associated with this sub step.

**mainStepUniqueId -** Refers to the main step's Id set on sub steps and subsub steps.

**buttonText -** Refers to the button visible on this current step.

**active -** Is this step/ component currently active.

**disabled -** Is this step/ component currently disabled.

**mainStepDisabled -** This is set on a sub step checking if  the main step disabled.

**mainDropdown -** Is this whole section open in a dropdown.

**dropdown -** Is this section open in a dropdown for a sub step.

**lastItem -** Is the very last item in the structure be it the main step, sub step or subsub step.





Inside the dynamic components to be loaded add the following additions:

**yourStep.component.ts**

```
//Input added
import { Component, Input } from '@angular/core';
//Stepper interface added from library
import { StepComponent } from 'nedbank-design';

//Implement's on StepComponent
 export class Step1Component implements StepComponent {
   //Data input
   @Input() data: any;
   constructor() { }
 }
```

You can access the data from the service for this component for example:

**yourStep.component.html**

```<h2>{{ data.stepName }}</h2>```


### Step 3 - Adding the stepper selector

You will need to add the following to the parent component (Component where the selector is used).

Import both the step-items and the service.

Create a var called steps and set it to the` StepItem[]`.

In the constructor inject the service.
Add -  `this.steps = this.stepperService.getSteps();`

You would also need the Save and Cancel modals Local variables **(stepperSaveModal and stepperCancelModal)** and emitted value **(modalInStepperValue)** from within the stepper.

Add these variables and the **modalInStepperCheck function** which will be the parent event handler.

**app.component.ts**

```

import { Component, OnInit} from '@angular/core';
import { StepItem } from 'nedbank-design';
import { StepperService } from './stepper.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  steps: StepItem[];
  
  //Value from modalCheck (from inside stepper)
  modalInStepperValue: boolean;

  //Local variables to check which modal should be shown
  stepperSaveModal = false;
  stepperCancelModal = false;


  constructor(private stepperService: StepperService) { }

  ngOnInit() {
    this.steps = this.stepperService.getSteps();
  }

  modalInStepperCheck(valueEmitted){
      this.modalInStepperValue = valueEmitted;

      // If save modal (true)
      if(this.modalInStepperValue === true){
        this.stepperSaveModal = true;
      }
      // If cancel modal (false)
      else {
        this.stepperCancelModal = true;
      }
  }
}



```



Then in this components html page or template you can use the stepper selector and modals.

 **app.component.html**

```
 <ui-stepper 
    [steps]="steps" 
    (modalCheck)="modalInStepperCheck($event)">
</ui-stepper>

<ui-modal 
[isShowModal]="stepperSaveModal"
(updateValue)="stepperSaveModal = $event" 
[hasIllustration]="false" 
modalId="Save"
modalHeading="Save your application" 
modalText="You should save your application for later." 
buttonText="Save" 
secondaryButtonText="Cancel"
></ui-modal>

<ui-modal 
[isShowModal]="stepperCancelModal"
(updateValue)="stepperCancelModal = $event" 
[hasIllustration]="false"
modalId="Cancel"
modalHeading="Cancel your application" 
modalText="You can cancel your application for now." 
buttonText="Cancel" 
secondaryButtonText="Continue"
></ui-modal>
```
  

The stepper selector will pull in the following html code:

```
<!-- Top nav bar -->
<nav>
 <svg class="logo" width="50px" height="50px" viewBox="0 0 50 50" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
   <g id="Landing-page-+-Nav" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
       <g id="Landing-Page-_-FINAL" transform="translate(-101.000000, -50.000000)" fill-rule="nonzero">
           <g id="NAVIGATION" transform="translate(0.000000, -3.000000)">
               <g id="Nedbank-Logo-Copy" transform="translate(101.000000, 53.000000)">
                   <polygon id="Fill-12" fill="#FFFFFF" points="25.139984 42.611336 13.9838096 50 0 40.9668969 0 9.03230927 13.9838096 0 25.139984 7.38866396 36.3131751 0 50 9.0505676 50 40.9494324 36.3131751 50"></polygon>
                   <path d="M14.3932863,32.2980623 L23.3756438,38.3566122 L14.3932863,44.4155617 L14.3932863,32.2980623 Z M48.0769231,10.2664627 L36.8491799,2.83018868 L25.6210295,10.2664627 L14.3932862,2.83018868 L2.88461539,10.2664627 L2.88461539,39.7339368 L14.3932862,47.1698114 L25.6210295,39.7339368 L36.8491799,47.1698114 L48.0769231,39.7339368 L48.0769231,10.2664627 Z M36.8491799,20.4557879 L36.8491799,5.58443821 L45.5510168,11.3681625 L45.5510168,38.0817865 L5.13040831,11.6429883 L14.3932862,5.58443821 L36.8491799,20.4557879 Z" id="Fill-14" fill="#006341"></path>
               </g>
           </g>
       </g>
   </g>
 </svg>
 <!-- Top nav counter -->
 <h6>Step {{currentMainStep}} of {{totalMainSteps}}</h6>
 <!-- Top nav counter End -->
 <!-- Right modal actions -->
 <div class="actions">
   <ui-hyperlink id="save" linkText="Save" linkUrl="#" iconName="nlsg-icon-save" (click)="launchModal($event)"></ui-hyperlink>
   <ui-hyperlink id="cancel" linkText="Cancel application" linkUrl="#" iconName="nlsg-icon-close" (click)="launchModal($event)"></ui-hyperlink>
 </div>
 <!-- Right modal actions End -->
 
 <div class="mobile-menu" (click)="mobileNav()">
   <i class="nlsg-c-icon nlsg-icon-steps nlsg-icon--size-24"></i>
 </div>
</nav>
<!-- Top nav bar End -->
 
<!-- Progress bar top -->
<div class="progress-bar-top">
 <div class="progress"></div>   
</div>
 
<div class="stepper-wrapper">
 <aside class="sidebar" [ngClass]="{'mobileNavShown': mobileNavOpen}">
     <div class="mobile-aside-wrapper">
       <!-- Progress bar side -->
       <div class="progress-bar progress-bar-side">
         <div class="progress"></div>   
       </div>
       <!-- Progress bar End -->
         <!-- Sidebar links -->
       <ul>
         <ng-container *ngFor="let step of allSteps;  let i = index, let first = first">
           <ng-container *ngIf="step.mainStep == true || step.dropdown == true">
             <li id="{{step.uniqueId}}" class="sidebar-link" value="{{step.order}}" [ngClass]="{'main-level' : step.mainStep == true}" *ngIf="step.mainStep || step.mainStepDisabled == false">
               <span *ngIf="step.mainStep" [ngClass]="{'activeClass' : step.active && !step.disabled, 'disabledStep' : step.disabled}" (click)="getStepsSideNav(i, $event)">{{step.stepName}}</span>
               <a class="dropdown-arrow" (click)="getStepsSideNav(i, $event)" *ngIf="step.mainStep" [ngClass]="{'activeClass' : step.active && !step.disabled, 'disabledStep' : step.disabled}">
                 <i class="nlsg-icon" [ngClass]="{'nlsg-icon-arrow-up' : first, 'nlsg-icon-arrow-down' : !first}"></i>
               </a>
                 <ul *ngIf="!step.mainStep && step.mainStepDisabled == false && step.dropdown === true">
                   <li class="sidebar-link" [ngClass]="{'sub-level' : step.subStepLevel1 == true}">
                       <span *ngIf="step.subStepLevel1" [ngClass]="{'activeClass' : step.active && !step.disabled, 'disabledStep' : step.disabled}" (click)="getStepsSideNav(i, $event)">{{step.stepName}}</span>
                       <ul>
                         <li class="sidebar-link sub-sub-level" *ngIf="!step.mainStep && !step.subStepLevel1">
                             <span *ngIf="step.subStepLevel2" [ngClass]="{'activeClass' : step.active && !step.disabled, 'disabledStep' : step.disabled}" (click)="getStepsSideNav(i, $event)">- {{step.stepName}}</span>
                         </li>
                       </ul>
                   </li>
                 </ul>
             </li>
           </ng-container>
         </ng-container>
       </ul>
       <!-- Sidebar links End -->
     </div>
     <!-- Right modal actions -->
     <div class="actions-mobile">
       <ui-hyperlink id="save" linkText="Save" linkUrl="#" iconName="nlsg-icon-save" (click)="launchModal($event)"></ui-hyperlink>
       <ui-hyperlink id="cancel" linkText="Cancel application" linkUrl="#" iconName="nlsg-icon-close" (click)="launchModal($event)"></ui-hyperlink>
     </div>
     <!-- Right modal actions End -->
 </aside>
 
 <!-- Stepper content -->
 <div class="stepper-content">
   <ng-template step-host>
   </ng-template>
 </div>
 <!-- Stepper content End -->
</div>
 
<!-- Stepper next button -->
<ui-button
 buttonText="{{stepButtonText}}"
 [disabled]="allSteps[currentStepIndex + 1].disabled && allSteps[currentStepIndex].active"
 (click)="getSteps()">
</ui-button>
<!-- Stepper next button End -->
 
```


This includes the top navigation bar with step counter and links Save and Cancel.

Side navigation with nested links and progress bar which shows the current step.

The anchor point where all dynamic components are loaded into as well as a button to navigate.



## Development details

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.1.1.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
