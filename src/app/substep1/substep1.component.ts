import { Component, Input } from '@angular/core';
import { StepComponent } from 'ANedbank';

@Component({
  selector: 'app-substep1',
  templateUrl: './substep1.component.html',
  styleUrls: ['./substep1.component.scss']
})
export class Substep1Component implements StepComponent {

  @Input() data: any;

}
