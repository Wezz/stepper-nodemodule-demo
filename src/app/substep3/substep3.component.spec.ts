import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Substep3Component } from './substep3.component';

describe('Substep3Component', () => {
  let component: Substep3Component;
  let fixture: ComponentFixture<Substep3Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Substep3Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Substep3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
