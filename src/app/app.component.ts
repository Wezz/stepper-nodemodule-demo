
import { Component, OnInit, HostListener} from '@angular/core';
import { StepItem } from 'ANedbank';
import { StepperService } from './stepper.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  steps: StepItem[];
  
  //Local variables to check which modal should be shown and saved block alert
  stepperSaveModal = false;
  stepperCancelModal = false;
  saveBlockAlert = false;
  
  applyFixed = false;
  hasAlert = false;

  constructor(private stepperService: StepperService) { }


  ngOnInit() {
    this.steps = this.stepperService.getSteps();
  }

  @HostListener('window:scroll')
	handleScroll(){
		const windowScroll = window.pageYOffset;
		if(windowScroll > 0){
      this.applyFixed = true;
		} else {
		  this.applyFixed = false;
		}
	}


  modalInStepperCheck(valueEmitted){

      // If save modal (true)
      if(valueEmitted === true){
        this.stepperSaveModal = true;
      }
      // If cancel modal (false)
      else {
        this.stepperCancelModal = true;
      }
  }

  // If alert has been closed from within
  alertCheck(valueEmitted){
    if(valueEmitted === false){
      this.saveBlockAlert = false;
      this.hasAlert = false;
    }
  }

  // Check if a primary or secondary button was pressed
  buttonInModalCheck(valueEmitted){
    if(valueEmitted == 'Primary'){
      this.primaryBtnFunction();
    }
    else {
      this.secondaryBtnFunction();
    }
  }

   // Check if a primary was pressed and if a save alert should be shown or not
  primaryBtnFunction(){
    if(this.stepperSaveModal == true){
      this.saveBlockAlert = true;
      this.stepperSaveModal = false;
      this.stepperCancelModal = false;
      this.hasAlert = true;
    }
    else {
      this.stepperSaveModal = false;
      this.stepperCancelModal = false;
    }
    
  }
  
   // Check if a secondary button was pressed
  secondaryBtnFunction(){
    this.stepperSaveModal = false;
    this.stepperCancelModal = false;
    this.saveBlockAlert = false;
    this.hasAlert = false;
  }
}


