import { Component, Input } from '@angular/core';
import { StepComponent } from 'ANedbank';

@Component({
  selector: 'app-step2',
  templateUrl: './step2.component.html',
  styleUrls: ['./step2.component.scss']
})
export class Step2Component implements StepComponent {

  constructor() { }
  @Input() data: any;
}
