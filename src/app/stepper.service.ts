import { Injectable } from '@angular/core';
import { StepItem } from 'ANedbank';
import { Step1Component } from './../app/step1/step1.component';
import { Substep1Component } from './../app/substep1/substep1.component';
import { Step2Component } from './../app/step2/step2.component';


@Injectable({
  providedIn: 'root'
})
export class StepperService {

  getSteps() {
    return [
      new StepItem(Step1Component,
      { uniqueId: 'step1main',
        order: 1,
        mainStep: true,
        stepName: 'Step 1',
        buttonText: 'Next to Sub step 1',
        active: false,
        disabled: false,
        mainDropdown: true,
        lastItem: false
        
      }),
      new StepItem(Substep1Component,
        { subStepLevel1: true,
          mainStepDisabled: false,
          MainstepName: 'Step 1',
          mainStepUniqueId: 'step1main',
          stepName: 'Sub step 1',
          buttonText: 'Next to step Sub sub step 1',
          active: false,
          disabled: false,
          dropdown: true,
          lastItem: false
      }),
      new StepItem(Substep1Component,
      { subStepLevel2: true,
        mainStepDisabled: false,
        MainstepName: 'Step 1',
        mainStepUniqueId: 'step1main',
        stepName: 'Sub sub step 1',
        buttonText: 'Next to step 2',
        active: false,
        disabled: false,
        dropdown: true,
        lastItem: false
      }),
      new StepItem(Step2Component,
      { uniqueId: 'step2main',
        order: 2,
        mainStep: true,
        stepName: 'Step 2',
        buttonText: 'Next to Sub step 2',
        active: false,
        disabled: false,
        mainDropdown: true,
        lastItem: false
        
      }),
      new StepItem(Substep1Component,
        { subStepLevel1: true,
          mainStepDisabled: false,
          MainstepName: 'Step 2',
          mainStepUniqueId: 'step2main',
          stepName: 'Sub step 2',
          buttonText: 'Next to step Sub sub step 2',
          active: false,
          disabled: false,
          dropdown: true,
          lastItem: false
      }),
      new StepItem(Substep1Component,
      { subStepLevel2: true,
        mainStepDisabled: false,
        MainstepName: 'Step 2',
        mainStepUniqueId: 'step2main',
        stepName: 'Sub sub step 2',
        buttonText: 'Next to step 3',
        active: false,
        disabled: false,
        dropdown: false,
        lastItem: false
      }),
      new StepItem(Step1Component,
      { uniqueId: 'step3main',
        order: 3,
        mainStep: true,
        stepName: 'Step 3',
        buttonText: 'Substep 3',
        active: false,
        disabled: false,
        mainDropdown: true,
        lastItem: false
      }),
      new StepItem(Substep1Component,
        { subStepLevel1: true,
          mainStepDisabled: false,
          MainstepName: 'Step 3',
          mainStepUniqueId: 'step3main',
          stepName: 'Substep 3',
          buttonText: 'Submit',
          active: false,
          disabled: false,
          dropdown: true,
          lastItem: true
      }),
    ];
  }
}
