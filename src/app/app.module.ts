import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { StepperModule, ButtonModule, ModalModule, BlockAlertModule } from 'ANedbank';

import { AppComponent } from './app.component';
import { Step1Component } from './step1/step1.component';
import { Substep1Component } from './substep1/substep1.component';
import { Subsubstep1Component } from './subsubstep1/subsubstep1.component';
import { Step2Component } from './step2/step2.component';
import { Step3Component } from './step3/step3.component';
import { Substep3Component } from './substep3/substep3.component';

@NgModule({
  declarations: [
    AppComponent,
    Step1Component,
    Substep1Component,
    Subsubstep1Component,
    Step2Component,
    Step3Component,
    Substep3Component,
  ],
  imports: [
    BrowserModule,
    ButtonModule,
    ModalModule,
    StepperModule,
    BlockAlertModule

  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [ Step1Component, Substep1Component, Subsubstep1Component, Step2Component],
})
export class AppModule { }
